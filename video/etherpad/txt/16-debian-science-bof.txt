Welcome to DebConf Video Etherpad!

This pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!

To get started click the 👥 button in the top/bottom right and log in.

Get involved with Etherpad at https://etherpad.org

I'm on https://jitsi.debian.social/DebianMedCovid19

* onetbb migration  ...  tille@debian.org (I'm just *suffering* - not filed the bugs ;-) ) can easily point people to relevant bugs!
* rocm-hipamd was accepted recently - what does it mean for science packages. hipcc.  Radeon Open Compute 
* connection to Debian AI (tensorflow & stuff)
* Debian math team
* Debian Astro (Thanks Ole for saying it was a good idea ;-) )  (Same in Debian Med with vanishing people - I have also no idea how to fix this since 20 years ;-) )
* vtk7 -> vtk9 migration (and drop vtk6!)
    I've just uploaded debian-science metapackages where I did the replacement
    
    -->  https://blends.debian.org/science/tasks/

* General topic: What about "team-orphaned" packages

* Regarding Salsa-CI:  Nilesh has written a script to trigger debian/salsa-ci.yaml for all repositories of a team  We were running this for Debian Med.  In addition the `routine-update` script creates a matching debian/salsa-ci.yaml file.   Sorry, no idea where NIlesh's script might be.  Routine-update is just a
     apt install routine-update
away.  The latter is relevant for every developer.  I can just ask Nilesh to run his script once.

Sponsoring -> please check whether upload is OK: builds, autopkgtests etc.

Remark to your slides:
I gues the statisitics "Uploaders per year" should rather be "Uploads per year" ;-)


