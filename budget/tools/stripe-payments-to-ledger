#!/usr/bin/env python3

from csv import DictReader
from decimal import Decimal


IGNORE = {'2022-00232', '2022-00234', '2022-00236', '2022-00237', '2022-00238',
          '2022-00239'}

class Payment:
    def __init__(self, row):
        self._raw_row = row

        self.created = self._raw_row['Created (UTC)']
        self.date = self.created.split()[0]

        self.amount = self._parse_value('Amount')
        self.converted_amount = self._parse_value('Converted Amount')
        self.amount_refunded = self._parse_value('Amount Refunded')
        self.fee = self._parse_value('Fee')

        self.currency = self._raw_row['Currency'].upper()
        self.converted_currency = self._raw_row['Converted Currency'].upper()

        self.description = self._raw_row['Description']

        self.breakdown = self._parse_breakdown()
        assert sum(self.breakdown.values()) == self.amount

    def _parse_value(self, column):
        return Decimal(self._raw_row[column].replace(',', '.'))

    def _parse_breakdown(self):
        breakdown = {}
        for category in ('accommodation', 'meals', 'registration'):
            breakdown[category] = self._parse_value(
                f'breakdown_{category} (metadata)')
        return breakdown

    def _fmt_posting(self, account, amount=None):
        if amount:
            return f'    {account:23} {amount:9.2f} {self.converted_currency}'
        else:
            return f'    {account}'

    def to_ledger(self):
        output = []
        output.append(f'{self.date} * {self.description}')
        if self.currency != self.converted_currency:
            output.append(f'    ; for {self.amount} {self.currency}')

        for category, value in self.breakdown.items():
            if not value:
                continue
            output.append(self._fmt_posting(f'income:{category}', -value))

        output.append(self._fmt_posting('expenses:fees', self.fee))

        if self.amount_refunded:
            output.append('    ; Refunded')
            if self.amount_refunded == self.amount - self.fee:
                for category, value in self.breakdown.items():
                    if not value:
                        continue
                    output.append(
                        self._fmt_posting(f'income:{category}', value))
                output.append('    ; Attendee covered the transaction fees')
                output.append(self._fmt_posting('expenses:fees', -self.fee))
            else:
                output.append(self._fmt_posting(
                    'expenses:refunds', self.amount_refunded))

        output.append(self._fmt_posting('assets:stripe'))

        return '\n'.join(output)


def main():
    payments = []
    with open('stripe/payments.csv') as f:
        r = DictReader(f)
        for row in r:
            if row['Description'].split()[-1] in IGNORE:
                continue
            payments.append(Payment(row))
    payments.sort(key=lambda p: p.created)
    for payment in payments:
        print(payment.to_ledger())
        print()


if __name__ == '__main__':
    main()
