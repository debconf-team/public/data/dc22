account assets:cash
account assets:debian-ch
account assets:debian-france
account assets:stripe
account assets:FLOSSK
account assets:ICTL
account assets:SPI

account expenses:accommodation:debcamp
account expenses:accommodation:debconf
account expenses:accommodation:bedding
account expenses:travel:bursary:diversity
account expenses:travel:bursary:general
account expenses:travel:invited-speakers
account expenses:travel:team:debconf-ctte
account expenses:travel:team:dc23
account expenses:travel:team:registration
account expenses:travel:team:video
account expenses:day trip:bus
account expenses:day trip:tour guide
account expenses:donations
account expenses:fees
account expenses:food:debcamp
account expenses:food:debconf
account expenses:food:refreshments
account expenses:graphic materials
account expenses:hacklab-bar
account expenses:incidentals
account expenses:local team
account expenses:party:cheese and wine
account expenses:party:conference dinner:bus
account expenses:party:conference dinner:food
account expenses:party:conference dinner:music
account expenses:party:social event
account expenses:refunds
account expenses:reimbursable:sponsor imports
account expenses:sponsor fulfilment
account expenses:swag
account expenses:tax
account expenses:venue:covid tests
account expenses:venue:hygiene
account expenses:venue:av equipment
account expenses:venue:internet
account expenses:venue:tents
account expenses:venue:rental
account expenses:video:computers
account expenses:video:shipping
account expenses:voip

account income:hacklab-bar
account income:accommodation
account income:meals
account income:registration
account income:frontdesk
account income:registration:stripe for debian-france
account income:registration:SEPA for debian-france
account income:sponsors:bronze
account income:sponsors:gold
account income:sponsors:platinum
account income:sponsors:silver
account income:sponsors:supporter
account income:sponsors:donations
account income:reimbursement:sponsor imports

; Create liability accounts for orga incurring expenses here
account liabilities:arianit
account liabilities:edonis
account liabilities:enkelena
account liabilities:nattie
account liabilities:olasd
account liabilities:pollo
account liabilities:stefanor
account liabilities:valmir
